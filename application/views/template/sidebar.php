<?php
$uri = $this->uri->segment(2);

if ($uri == 'home') {
    $dashboard  = 'active';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'meta') {
    $dashboard  = '';
    $meta       = 'active';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'header') {
    $dashboard  = '';
    $meta       = '';
    $header     = 'active';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'about') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = 'active';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'profil') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = 'active';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'education') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = 'active';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'experience') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = 'active';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'skill') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = 'active';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'more') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = 'active';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'work') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = 'active';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'social') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = 'active';
    $message    = '';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'message') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = 'active';
    $privacy    = '';
    $users      = '';
} elseif ($uri == 'privacy') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = 'active';
    $users      = '';
} elseif ($uri == 'users') {
    $dashboard  = '';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = 'active';
} else {
    $dashboard  = 'active';
    $meta       = '';
    $header     = '';
    $about      = '';
    $profil     = '';
    $education  = '';
    $experience = '';
    $skill      = '';
    $more       = '';
    $work       = '';
    $social     = '';
    $message    = '';
    $privacy    = '';
    $users      = '';
}
?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <br><br>
            <li class="sidebar-search-wrapper">
            </li>

            <li class="tooltips <?=$dashboard;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Dashboard">
                <a href="<?=site_url('admin/home');?>">
                    <i class="fa fa-home"></i><span class="title"> Dashboard</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">TITLE</h3>
            </li>
            <li class="tooltips <?=$meta;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Meta Tags & SEO">
                <a href="<?=site_url('admin/meta');?>">
                    <i class="fa fa-search"></i><span class="title"> Meta Tags & SEO</span>
                </a>
            </li>
            <li class="tooltips <?=$header;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Setting Title">
                <a href="<?=site_url('admin/header');?>">
                    <i class="fa fa-cog"></i><span class="title"> Setting Title</span>
                </a>
            </li>
            <li class="tooltips <?=$about;?>" data-container="body" data-placement="right" data-html="true" data-original-title="About Me">
                <a href="<?=site_url('admin/about');?>">
                    <i class="fa fa-external-link"></i><span class="title"> About Me</span>
                </a>
            </li>
            <li class="tooltips <?=$profil;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Profil">
                <a href="<?=site_url('admin/profil');?>">
                    <i class="fa fa-user"></i><span class="title"> Profil</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">CONTENT</h3>
            </li>
            <li class="tooltips <?=$education;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Education">
                <a href="<?=site_url('admin/education');?>">
                    <i class="fa fa-university"></i><span class="title"> Education</span>
                </a>
            </li>
            <li class="tooltips <?=$experience;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Experience">
                <a href="<?=site_url('admin/experience');?>">
                    <i class="fa fa-graduation-cap"></i><span class="title"> Experience</span>
                </a>
            </li>
            <li class="tooltips <?=$skill;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Skills">
                <a href="<?=site_url('admin/skill');?>">
                    <i class="fa fa-asterisk"></i><span class="title"> Skills</span>
                </a>
            </li>
            <li class="tooltips <?=$more;?>" data-container="body" data-placement="right" data-html="true" data-original-title="More Skills">
                <a href="<?=site_url('admin/more');?>">
                    <i class="fa fa-anchor"></i><span class="title"> More Skills</span>
                </a>
            </li>
            <li class="tooltips <?=$work;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Works">
                <a href="<?=site_url('admin/work');?>">
                    <i class="fa fa-tasks"></i><span class="title"> Works</span>
                </a>
            </li>
            <li class="tooltips <?=$social;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Social Media">
                <a href="<?=site_url('admin/social');?>">
                    <i class="fa fa-spinner"></i><span class="title"> Social Media</span>
                </a>
            </li>
            <li class="tooltips <?=$message;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Message">
                <a href="<?=site_url('admin/message');?>">
                    <i class="fa fa-comments"></i><span class="title"> Message</span>
                </a>
            </li>
            <li class="tooltips <?=$privacy;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Privacy Policy">
                <a href="<?=site_url('admin/privacy');?>">
                    <i class="fa fa-exclamation-circle"></i><span class="title"> Privacy Policy</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">USERS</h3>
            </li>
            <li class="tooltips <?=$users;?>" data-container="body" data-placement="right" data-html="true" data-original-title="Users">
                <a href="<?=site_url('admin/users');?>">
                    <i class="fa fa-users"></i><span class="title"> Users</span>
                </a>
            </li>
        </ul>
    </div>
</div>