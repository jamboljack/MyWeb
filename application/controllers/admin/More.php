<?php
defined('BASEPATH') or exit('No direct script access allowed');

class More extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in_me')) {
            redirect(base_url());
        }

        $this->load->library('template');
        $this->load->model('admin/more_m');
    }

    public function index()
    {
        if ($this->session->userdata('logged_in_me')) {
            $this->template->display('admin/master/more_view');
        } else {
            $this->session->sess_destroy();
            redirect(base_url());
        }
    }

    public function data_list()
    {
        $List = $this->more_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row     = array();
            $more_id = $r->more_id;

            $row[] = '  <button type="button" class="btn btn-primary btn-xs" title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $more_id . "'" . ')">
                        <i class="fa fa-edit"></i>
                        </button>
                        <a onclick="hapusData(' . $more_id . ')">
                        <button class="btn btn-danger btn-xs" type="button" title="Delete Data">
                        <i class="fa fa-times-circle"></i>
                        </button>
                        </a>';

            $row[] = $no;
            $row[] = $r->more_name;
            $row[] = $r->more_percent;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->more_m->count_all(),
            "recordsFiltered" => $this->more_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function savedata()
    {
        $this->more_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->more_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {

        $this->more_m->update_data();
    }

    public function deletedata($id)
    {
        $this->more_m->delete_data($id);
        echo json_encode(array("status" => true));
    }
}
/* Location: ./application/controller/admin/More.php */
