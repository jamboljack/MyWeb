-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 14, 2019 at 11:39 AM
-- Server version: 10.2.24-MariaDB-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `semarsak_jamamuttaqin`
--

-- --------------------------------------------------------

--
-- Table structure for table `my_education`
--

CREATE TABLE `my_education` (
  `education_id` int(2) NOT NULL,
  `education_no` int(2) NOT NULL DEFAULT 1,
  `education_name` varchar(50) NOT NULL COMMENT 'Nama Sekolah',
  `education_level` varchar(30) NOT NULL COMMENT 'Level Sekolaj',
  `education_date` varchar(10) NOT NULL COMMENT 'Tahun',
  `education_desc` text NOT NULL COMMENT 'Deskripsi',
  `education_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_education`
--

INSERT INTO `my_education` (`education_id`, `education_no`, `education_name`, `education_level`, `education_date`, `education_desc`, `education_update`) VALUES
(1, 1, 'SD N 04 UNDAAN KIDUL', 'PRIMARY SCHOOL', '1993-1994', 'Elementary School in the Village where my birth Village Undaan Kidul, District Undaan', '2018-01-29 14:47:26'),
(2, 1, 'MTS NAHDLATUL MUSLIMIN', 'JUNIOR HIGH SCHOOL', '1999-2002', 'Islamic School is the equivalent of junior high school, many join the Organization. From OSIS and Scouting.', '2018-01-29 14:46:25'),
(3, 1, 'SMA N 2 KUDUS', 'SENIOR HIGH SCHOOL', '2002-2005', 'A fun high school school, lots of extracurriculars and organizations. Ever been a sacred representative in the cultural camp as well as the Final of soccer between schools.', '2018-01-29 14:48:55'),
(4, 1, 'UNIVERSITAS MURIA KUDUS', 'COLLEGE', '2006-2010', 'Start a programmer career in college, by following and apprenticeship and maximizing opportunities. Receive small-scale projects to assist Data logic and analysis.', '2018-01-29 22:44:06');

-- --------------------------------------------------------

--
-- Table structure for table `my_experience`
--

CREATE TABLE `my_experience` (
  `experience_id` int(2) NOT NULL,
  `experience_no` int(2) NOT NULL DEFAULT 1,
  `experience_name` varchar(50) NOT NULL COMMENT 'Nama Jabatan',
  `experience_place` varchar(50) NOT NULL COMMENT 'Nama Perusahaan',
  `experience_date` varchar(15) NOT NULL COMMENT 'Tahun',
  `experience_desc` text NOT NULL COMMENT 'Deskripsi',
  `experience_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_experience`
--

INSERT INTO `my_experience` (`experience_id`, `experience_no`, `experience_name`, `experience_place`, `experience_date`, `experience_desc`, `experience_update`) VALUES
(1, 1, 'LECTURE ASSISTANT', 'UNIVERSITAS MURIA KUDUS', '2008-2010', 'Assisting lecturers in practical lectures in laboratories', '2018-01-29 15:20:15'),
(2, 1, 'PROGRAMMER FREELANCE', 'CV. PRO REKA INTI', '2008-2010', 'Creating desktop-based applications, using Ms. Visual Foxpro 9. Assisting projects in PLN Kabupaten Kudus (Application Data Trafo)', '2018-01-29 15:09:38'),
(3, 1, 'PROGRAMMER', 'PT. VISIONE SYSTEM &#40;SOLO&#41;', '2010-2011', 'Creating desktop-based payroll apps, desktop-based retail store apps, mobile phone store apps.', '2018-01-29 23:55:41'),
(4, 1, 'PROGRAMMER', 'CV. VISUAL SOLUTION (SOLO)', '2011-2011', 'Focus on creating V-Mobile app (application for mobile phone shop and mobile phone repair service)', '2018-01-29 15:13:52'),
(5, 1, 'IT MANAGER', 'RSU KUMALA SIWI (KUDUS)', '2011-2012', 'Responsible for smooth hospital system &#40;SIM-RS&#41;, network, internet, hardware and software. Create apps as needed from departmental requests.', '2018-01-29 23:56:28'),
(6, 1, 'IT OFFICER', '@HOM HOTEL BY HORISON (KUDUS)', '2013 - 2018', 'Responsible for the smoothness of Hotel Application System &#40;HRIS&#41;, Internet and Wifi, Hardware and Software.', '2018-03-05 10:12:40'),
(7, 1, 'PHP DEVELOPER', 'PT. HUMANIKA MITRA SOLUSI', '2015 - PRESENT', 'Create a web-based information system application for district and private government projects', '2018-03-23 14:32:27');

-- --------------------------------------------------------

--
-- Table structure for table `my_message`
--

CREATE TABLE `my_message` (
  `message_id` int(10) NOT NULL,
  `message_name` varchar(50) NOT NULL,
  `message_email` varchar(50) NOT NULL,
  `message_subject` varchar(70) NOT NULL,
  `message_message` text DEFAULT NULL,
  `message_status` int(1) DEFAULT 1 COMMENT '1. Belum 2. Sudah',
  `message_post` datetime NOT NULL COMMENT 'Tgl. Kirim',
  `message_read` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_message`
--

INSERT INTO `my_message` (`message_id`, `message_name`, `message_email`, `message_subject`, `message_message`, `message_status`, `message_post`, `message_read`) VALUES
(1, 'Hkjnb', 'ghhhh@gjj.com', 'Ghjjhh', 'GhjjnGhjjnnjj', 1, '2018-06-01 06:35:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `my_meta`
--

CREATE TABLE `my_meta` (
  `meta_id` int(2) NOT NULL,
  `meta_name` varchar(50) NOT NULL COMMENT 'Nama Website',
  `meta_desc` text DEFAULT NULL,
  `meta_keyword` text DEFAULT NULL,
  `meta_author` varchar(100) DEFAULT NULL,
  `meta_developer` varchar(50) DEFAULT NULL,
  `meta_robots` varchar(50) DEFAULT NULL,
  `meta_googlebots` varchar(50) DEFAULT NULL,
  `meta_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_meta`
--

INSERT INTO `my_meta` (`meta_id`, `meta_name`, `meta_desc`, `meta_keyword`, `meta_author`, `meta_developer`, `meta_robots`, `meta_googlebots`, `meta_update`) VALUES
(1, 'IAMJ - Personal CV', 'Website Curriculum Vitae Personal', 'jama,muttaqin,programmer,developer,php,codeigniter,ci,ci3,web,desain,sistem,aplikasi,informasi,sistem informasi,kudus,sublime,code,coding,coder', 'Jama&#039; Rochmad Muttaqin', 'Jama&#039; Rochmad Muttaqin', 'index, follow', 'index, follow', '2018-01-30 20:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `my_more`
--

CREATE TABLE `my_more` (
  `more_id` int(2) NOT NULL,
  `more_name` varchar(30) NOT NULL,
  `more_percent` int(10) NOT NULL DEFAULT 0,
  `more_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_more`
--

INSERT INTO `my_more` (`more_id`, `more_name`, `more_percent`, `more_update`) VALUES
(1, 'MOTIVATION', 95, '2018-01-29 17:33:30'),
(2, 'LEADERSHIP', 70, '2018-01-29 17:33:01'),
(3, 'MANAGEMENT', 85, '2018-01-29 17:33:11'),
(4, 'CREATIVITY', 70, '2018-01-29 17:33:21'),
(5, 'MARKETING', 75, '2018-01-29 17:33:43'),
(6, 'BRANDING', 70, '2018-01-29 17:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `my_privacy`
--

CREATE TABLE `my_privacy` (
  `privacy_id` int(2) NOT NULL,
  `privacy_desc` text NOT NULL,
  `privacy_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_privacy`
--

INSERT INTO `my_privacy` (`privacy_id`, `privacy_desc`, `privacy_update`) VALUES
(1, '<b>KEBIJAKAN PRIVASI<br></b><br><b>PENGANTAR<br></b>Aplikasi SIM-RTLH Android merupakan Aplikasi yang bertujuan membantu Operator dan Fasilitator dalam melaksanakan tugas dilapangan. <br><b><br>INFORMASI YANG DIKUMPULKAN<br></b>Aplikasi ini mengumpulkan data untuk pengajuan bantuan perumahan di Dinas Perumahan dan Permukiman Kabupaten Jepara. Mulai dari Data diri, Foto, Letak Lokasi rumah dan sebagainya.<br><br><b>PERUBAHAN TERHADAP KEBIJAKAN PERIVASI<br></b>Anda setuju bahwa Aplikasi ini bisa merubah kebijakan privasi ini setiap saat.<br><b><br>INFORMASI KONTAK<br></b>Jika Anda memiliki pertanyaan atau saran mengenai Kebijakan Privasi kami, silahkan hubungi kami melalui halaman kontak', '2018-03-05 09:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `my_profil`
--

CREATE TABLE `my_profil` (
  `profil_id` int(1) NOT NULL,
  `profil_name` varchar(50) NOT NULL,
  `profil_place` varchar(30) NOT NULL,
  `profil_date` date NOT NULL,
  `profil_address` varchar(100) NOT NULL,
  `profil_nation` varchar(20) NOT NULL,
  `profil_phone` varchar(12) NOT NULL,
  `profil_email` varchar(30) NOT NULL,
  `profil_image` varchar(100) NOT NULL,
  `profil_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_profil`
--

INSERT INTO `my_profil` (`profil_id`, `profil_name`, `profil_place`, `profil_date`, `profil_address`, `profil_nation`, `profil_phone`, `profil_email`, `profil_image`, `profil_update`) VALUES
(1, 'JAMA&#039; ROCHMAD MUTTAQIN', 'KUDUS', '1987-07-21', 'DUSUN TERSONO RT. 02/03 DESA GARUNG LOR, KALIWUNGU - KUDUS 59361', 'INDONESIA', '085640969727', 'jama.muttaqin@gmail.com', 'Profil_jama039-rochmad-muttaqin_1517203145.jpg', '2018-01-29 22:43:57');

-- --------------------------------------------------------

--
-- Table structure for table `my_setting`
--

CREATE TABLE `my_setting` (
  `setting_id` int(1) NOT NULL,
  `setting_title` varchar(50) NOT NULL,
  `setting_subtitle` varchar(100) NOT NULL,
  `setting_desc` text NOT NULL,
  `setting_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_setting`
--

INSERT INTO `my_setting` (`setting_id`, `setting_title`, `setting_subtitle`, `setting_desc`, `setting_update`) VALUES
(1, 'I AM J', 'FREELANCE <SPAN>DEVELOPER</SPAN>', 'I am a Professional Freelance Developer', '2018-03-23 14:32:58'),
(2, 'About Us', '', '<h3>OBJECTIVE</h3>I am someone who loves and is interested about programming since high school, following the development of information technology until today. Started programming since I got programming language course, and continue to hone myself ability from year to year. Focus on the back end programmer and know little about the front end, sharpen logic and data analysis for several years until college and work. <br><br><h3>WHAT I DO ?</h3>Now I work as a freelancer in a company that handles system application in district service and employees in a Hotel with famous brand in Indonesia. I am hardworking, focused, sometimes funny and responsible for what I do. I’m a forward thinker, which others may find inspiring when working as a team.', '2018-01-29 23:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `my_skill`
--

CREATE TABLE `my_skill` (
  `skill_id` int(2) NOT NULL,
  `skill_name` varchar(30) NOT NULL,
  `skill_percent` int(10) NOT NULL DEFAULT 0,
  `skill_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_skill`
--

INSERT INTO `my_skill` (`skill_id`, `skill_name`, `skill_percent`, `skill_update`) VALUES
(1, 'PHP', 80, '2018-01-29 17:26:30'),
(2, 'MS. V. FOXPRO', 80, '2018-01-29 23:43:36'),
(3, 'MYSQL', 70, '2018-01-29 17:26:38'),
(4, 'SQL SERVER', 50, '2018-01-29 17:26:45'),
(5, 'CODEIGNITER', 70, '2018-01-29 17:26:56'),
(6, 'WIN. SERVER', 40, '2018-01-29 23:43:51'),
(7, 'REST API', 50, '2018-01-29 17:27:16'),
(8, 'NETWORKING', 50, '2018-01-29 17:27:33'),
(9, 'CSS', 40, '2018-01-30 20:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `my_social`
--

CREATE TABLE `my_social` (
  `social_id` int(2) NOT NULL,
  `social_name` varchar(30) NOT NULL,
  `social_class` varchar(100) NOT NULL,
  `social_url` text NOT NULL,
  `social_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_social`
--

INSERT INTO `my_social` (`social_id`, `social_name`, `social_class`, `social_url`, `social_update`) VALUES
(1, 'FACEBOOK', 'facebook', 'https://www.facebook.com/jamamuttaqin', '2018-01-29 17:41:01'),
(2, 'INSTAGRAM', 'instagram', 'https://www.instagram.com/jama.muttaqin', '2018-01-30 17:04:41'),
(3, 'TWITTER', 'twitter', 'https://twitter.com/Jama_Muttaqin', '2018-01-29 17:40:58'),
(4, 'GITHUB', 'github', 'https://github.com/jamboljack', '2018-03-23 21:46:58');

-- --------------------------------------------------------

--
-- Table structure for table `my_users`
--

CREATE TABLE `my_users` (
  `user_username` varchar(30) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_mobile` varchar(12) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_status` enum('Aktif','Tidak Aktif') NOT NULL DEFAULT 'Aktif',
  `user_avatar` varchar(100) DEFAULT NULL COMMENT 'Avatar',
  `user_level` enum('Admin','Operator') NOT NULL DEFAULT 'Operator' COMMENT 'Level Admin',
  `user_date_create` datetime NOT NULL,
  `user_date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_users`
--

INSERT INTO `my_users` (`user_username`, `user_password`, `user_name`, `user_mobile`, `user_email`, `user_status`, `user_avatar`, `user_level`, `user_date_create`, `user_date_update`) VALUES
('admin', '7712fcffc21bb7215f58035ab4506a5873c4af3c', 'Administrator', '085640969727', 'jama.muttaqin@gmail.com', 'Aktif', 'Avatar_admin_1516902685.jpg', 'Admin', '0000-00-00 00:00:00', '2018-01-29 20:38:51');

-- --------------------------------------------------------

--
-- Table structure for table `my_work`
--

CREATE TABLE `my_work` (
  `work_id` int(2) NOT NULL,
  `work_name` varchar(50) NOT NULL,
  `work_url` varchar(100) NOT NULL,
  `work_image` varchar(100) NOT NULL,
  `work_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `my_work`
--

INSERT INTO `my_work` (`work_id`, `work_name`, `work_url`, `work_image`, `work_update`) VALUES
(1, 'BE-HRD (PAYROLL)', '', 'Work_1517238362.jpg', '2018-01-29 22:33:46'),
(2, 'E-FILE &#40;KAB. JEPARA&#41;', '', 'Work_1517239142.jpg', '2018-01-29 22:49:14'),
(3, 'FUTSAL PRO (MARKASS STADIUM)', '', 'Work_1517241387.jpg', '2018-01-29 22:56:27'),
(4, 'MOBIPOS (RETAIL)', '', 'Work_1517321256.jpg', '2018-01-30 21:07:36'),
(5, 'SIMAIL', '', 'Work_1517321395.jpg', '2018-01-30 21:09:55'),
(6, 'SEMAR SAKTI ART', 'https://www.semarsakti-art.com/', 'Work_1517321515.jpg', '2018-01-30 21:19:05'),
(7, 'SEMAR SAKTI FURNITURE', 'http://www.semar-furniture.com/', 'Work_1517322249.jpg', '2018-01-30 21:24:09'),
(8, 'FAKO (UD. JATI NUGROHO)', '', 'Work_1517322875.jpg', '2018-01-30 21:34:35'),
(9, 'GRIYA PRAPANCA APARTMENT', 'http://www.griyaprapanca.com/', 'Work_1517323183.jpg', '2018-01-30 21:41:42'),
(10, '@HOM KUDUS BY HORISON', 'http://www.hotelhomkudus.com', 'Work_1517323379.jpg', '2018-01-30 21:42:59'),
(11, 'RUMAH KECILKU DAYCARE', 'http://www.daycarerumahkecilku.com/', 'Work_1517323510.jpg', '2018-01-30 21:45:10'),
(12, 'RS AISYIYAH KUDUS', 'http://www.rsaisyiyah.com/', 'Work_1517323743.jpg', '2018-01-30 21:49:03'),
(13, 'TOKO APLUS JAKARTA', 'http://www.tokoaplus.com/', 'Work_1517323886.jpg', '2018-01-30 21:51:26'),
(14, 'ALUMNI TRACER UMK', 'http://alumni.umk.ac.id/', 'Work_1517324173.jpg', '2018-01-30 21:56:13'),
(15, 'DPM PTSP KUDUS', 'http://bpmppt.kuduskab.go.id', 'Work_1517324608.jpg', '2019-04-02 10:34:10'),
(16, 'KCFURNINDO JEPARA', 'https://www.kcfurnindo.com/', 'Work_1517324747.jpg', '2018-01-30 22:05:47'),
(17, 'SIPP (E-RETRIBUSI PASAR)', '', 'Work_1517324997.jpg', '2018-01-30 22:09:57'),
(18, 'LANGGARDALEM KUDUS', 'http://langgardalem.id/', 'Work_1517325088.jpg', '2018-01-30 22:11:28'),
(19, 'SIMPERKIM KAB. JEPARA', '', 'Work_1517325160.jpg', '2018-01-30 22:12:40'),
(20, 'SIM-RTLH KAB. JEPARA', 'https://sim-rtlh.jepara.go.id', 'Work_1517325351.jpg', '2019-04-02 10:34:34'),
(21, 'PASARKU UMKM KUDUS', 'https://pasarku.kuduskab.go.id/', 'Work_1517325415.jpg', '2018-01-30 22:16:55'),
(22, 'ALIF-A DAYCARE', 'http://www.alifadaycare.com/', 'Work_1517325555.jpg', '2018-01-30 22:19:15'),
(23, 'APPCARD', '', 'Work_1528167552.jpg', '2018-06-05 09:59:12'),
(24, 'SD KHALIFAH JOGJA', 'http://www.sdkhalifahjogja.sch.id/', 'Work_1554176288.jpg', '2019-04-02 10:38:10'),
(25, 'SIPASAR | E-RETRIBUSI', '', 'Work_1554176425.jpg', '2019-04-02 10:47:04'),
(26, 'PORTAL BAWASLU KAB. KUDUS', '', 'Work_1554176519.jpg', '2019-04-02 10:46:33'),
(27, 'PORTAL DISKOMINFO KAB. KUDUS', 'http://diskominfo.kuduskab.go.id', 'Work_1554176655.jpg', '2019-04-02 10:44:15'),
(28, 'SI-DICA | CETAK KARTU ASN', '', 'Work_1554176774.jpg', '2019-04-02 10:46:14'),
(29, 'SIMPEG KUDUS', '', 'Work_1554176896.jpg', '2019-04-02 10:48:16'),
(30, 'HORAIOS MALIOBORO JOGJA', 'http://www.horaioshotel.com/', 'Work_1554177314.jpg', '2019-04-02 10:55:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `my_education`
--
ALTER TABLE `my_education`
  ADD PRIMARY KEY (`education_id`),
  ADD KEY `education_name` (`education_name`);

--
-- Indexes for table `my_experience`
--
ALTER TABLE `my_experience`
  ADD PRIMARY KEY (`experience_id`),
  ADD KEY `experience_no` (`experience_no`),
  ADD KEY `experience_name` (`experience_name`);

--
-- Indexes for table `my_message`
--
ALTER TABLE `my_message`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `message_post` (`message_post`);

--
-- Indexes for table `my_meta`
--
ALTER TABLE `my_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `my_more`
--
ALTER TABLE `my_more`
  ADD PRIMARY KEY (`more_id`),
  ADD KEY `more_name` (`more_name`);

--
-- Indexes for table `my_privacy`
--
ALTER TABLE `my_privacy`
  ADD PRIMARY KEY (`privacy_id`);

--
-- Indexes for table `my_profil`
--
ALTER TABLE `my_profil`
  ADD PRIMARY KEY (`profil_id`);

--
-- Indexes for table `my_setting`
--
ALTER TABLE `my_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `my_skill`
--
ALTER TABLE `my_skill`
  ADD PRIMARY KEY (`skill_id`),
  ADD KEY `skill_name` (`skill_name`);

--
-- Indexes for table `my_social`
--
ALTER TABLE `my_social`
  ADD PRIMARY KEY (`social_id`),
  ADD KEY `social_name` (`social_name`);

--
-- Indexes for table `my_users`
--
ALTER TABLE `my_users`
  ADD PRIMARY KEY (`user_username`);

--
-- Indexes for table `my_work`
--
ALTER TABLE `my_work`
  ADD PRIMARY KEY (`work_id`),
  ADD KEY `work_name` (`work_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `my_education`
--
ALTER TABLE `my_education`
  MODIFY `education_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `my_experience`
--
ALTER TABLE `my_experience`
  MODIFY `experience_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `my_message`
--
ALTER TABLE `my_message`
  MODIFY `message_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `my_meta`
--
ALTER TABLE `my_meta`
  MODIFY `meta_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `my_more`
--
ALTER TABLE `my_more`
  MODIFY `more_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `my_privacy`
--
ALTER TABLE `my_privacy`
  MODIFY `privacy_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `my_profil`
--
ALTER TABLE `my_profil`
  MODIFY `profil_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `my_setting`
--
ALTER TABLE `my_setting`
  MODIFY `setting_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `my_skill`
--
ALTER TABLE `my_skill`
  MODIFY `skill_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `my_social`
--
ALTER TABLE `my_social`
  MODIFY `social_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `my_work`
--
ALTER TABLE `my_work`
  MODIFY `work_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
