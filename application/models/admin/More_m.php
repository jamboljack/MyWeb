<?php
defined('BASEPATH') or exit('No direct script access allowed');

class More_m extends CI_Model
{
    public $table         = 'my_more';
    public $column_order  = array(null, null, 'more_name', 'more_percent');
    public $column_search = array('more_name', 'more_percent');
    public $order         = array('more_id' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function insert_data()
    {
        $data = array(
            'more_name'    => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
            'more_percent' => $this->input->post('percent', 'true'),
            'more_update'  => date('Y-m-d H:i:s'),
        );

        $this->db->insert('my_more', $data);
    }

    public function select_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('my_more');
        $this->db->where('more_id', $id);

        return $this->db->get();
    }

    public function update_data()
    {
        $more_id = $this->input->post('id', 'true');

        $data = array(
            'more_name'    => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
            'more_percent' => $this->input->post('percent', 'true'),
            'more_update'  => date('Y-m-d H:i:s'),
        );
        $this->db->where('more_id', $more_id);
        $this->db->update('my_more', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('more_id', $id);
        $this->db->delete('my_more');
    }
}
/* Location: ./application/models/admin/More_m.php */
