<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Header_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function select_detail($setting_id = 1)
    {
        $this->db->select('*');
        $this->db->from('my_setting');
        $this->db->where('setting_id', $setting_id);

        return $this->db->get();
    }

    public function update_data()
    {
        $setting_id = 1;

        $data = array(
            'setting_title'    => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
            'setting_subtitle' => strtoupper($this->input->post('subname', 'true')),
            'setting_desc'     => $this->input->post('desc', 'true'),
            'setting_update'   => date('Y-m-d H:i:s'),
        );

        $this->db->where('setting_id', $setting_id);
        $this->db->update('my_setting', $data);
    }
}
/* Location: ./application/models/admin/Header_m.php */
