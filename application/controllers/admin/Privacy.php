<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Privacy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in_me')) {
            redirect(base_url());
        }

        $this->load->library('template');
        $this->load->model('admin/privacy_m');
    }

    public function index()
    {
        if ($this->session->userdata('logged_in_me')) {
            $data['detail'] = $this->privacy_m->select_detail()->row();
            $this->template->display('admin/master/privacy_view', $data);
        } else {
            $this->session->sess_destroy();
            redirect(base_url());
        }
    }

    public function updatedata()
    {
        $this->privacy_m->update_data();
    }
}

/* Location: ./application/controller/admin/Privacy.php */
