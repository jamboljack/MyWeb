<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Skill extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in_me')) {
            redirect(base_url());
        }

        $this->load->library('template');
        $this->load->model('admin/skill_m');
    }

    public function index()
    {
        if ($this->session->userdata('logged_in_me')) {
            $this->template->display('admin/master/skill_view');
        } else {
            $this->session->sess_destroy();
            redirect(base_url());
        }
    }

    public function data_list()
    {
        $List = $this->skill_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row     = array();
            $skill_id = $r->skill_id;

            $row[] = '  <button type="button" class="btn btn-primary btn-xs" title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $skill_id . "'" . ')">
                        <i class="fa fa-edit"></i>
                        </button>
                        <a onclick="hapusData(' . $skill_id . ')">
                        <button class="btn btn-danger btn-xs" type="button" title="Delete Data">
                        <i class="fa fa-times-circle"></i>
                        </button>
                        </a>';

            $row[] = $no;
            $row[] = $r->skill_name;
            $row[] = $r->skill_percent;

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->skill_m->count_all(),
            "recordsFiltered" => $this->skill_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function savedata()
    {
        $this->skill_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->skill_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {

        $this->skill_m->update_data();
    }

    public function deletedata($id)
    {
        $this->skill_m->delete_data($id);
        echo json_encode(array("status" => true));
    }
}
/* Location: ./application/controller/admin/Skill.php */
