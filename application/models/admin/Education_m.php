<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Education_m extends CI_Model
{
    public $table         = 'my_education';
    public $column_order  = array(null, null, 'education_date', 'education_name', 'education_level', 'education_desc');
    public $column_search = array('education_name', 'education_level', 'education_date', 'education_desc');
    public $order         = array('education_id' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }

            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function insert_data()
    {
        $data = array(
            'education_name'   => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
            'education_level'  => strtoupper(stripHTMLtags($this->input->post('level', 'true'))),
            'education_date'   => strtoupper(stripHTMLtags($this->input->post('date', 'true'))),
            'education_desc'   => trim(stripHTMLtags($this->input->post('desc', 'true'))),
            'education_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('my_education', $data);
    }

    public function select_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('my_education');
        $this->db->where('education_id', $id);

        return $this->db->get();
    }

    public function update_data()
    {
        $education_id = $this->input->post('id', 'true');

        $data = array(
            'education_name'   => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
            'education_level'  => strtoupper(stripHTMLtags($this->input->post('level', 'true'))),
            'education_date'   => strtoupper(stripHTMLtags($this->input->post('date', 'true'))),
            'education_desc'   => trim(stripHTMLtags($this->input->post('desc', 'true'))),
            'education_update' => date('Y-m-d H:i:s'),
        );
        $this->db->where('education_id', $education_id);
        $this->db->update('my_education', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('education_id', $id);
        $this->db->delete('my_education');
    }
}
/* Location: ./application/models/admin/Education_m.php */
