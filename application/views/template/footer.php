<div class="page-footer">
	<div class="page-footer-inner">
		<?=date('Y');?> &copy; Me is Programmer
	</div>
	<div class="scroll-to-top">
	    <i class="icon-arrow-up"></i>
	</div>
</div>