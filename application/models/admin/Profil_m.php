<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profil_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function select_detail($profil_id = 1)
    {
        $this->db->select('*');
        $this->db->from('my_profil');
        $this->db->where('profil_id', $profil_id);

        return $this->db->get();
    }

    public function update_data()
    {
        $profil_id = 1;
        if (!empty($_FILES['foto']['name'])) {
            $data = array(
                'profil_name'    => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
                'profil_place'   => strtoupper(stripHTMLtags($this->input->post('place', 'true'))),
                'profil_date'    => $this->input->post('date', 'true'),
                'profil_address' => strtoupper(stripHTMLtags($this->input->post('address', 'true'))),
                'profil_nation'  => strtoupper(stripHTMLtags($this->input->post('nation', 'true'))),
                'profil_phone'   => trim(stripHTMLtags($this->input->post('phone', 'true'))),
                'profil_email'   => trim(stripHTMLtags($this->input->post('email', 'true'))),
                'profil_image'   => $this->upload->file_name,
                'profil_update'  => date('Y-m-d H:i:s'),
            );
        } else {
            $data = array(
                'profil_name'    => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
                'profil_place'   => strtoupper(stripHTMLtags($this->input->post('place', 'true'))),
                'profil_date'    => $this->input->post('date', 'true'),
                'profil_address' => strtoupper(stripHTMLtags($this->input->post('address', 'true'))),
                'profil_nation'  => strtoupper(stripHTMLtags($this->input->post('nation', 'true'))),
                'profil_phone'   => trim(stripHTMLtags($this->input->post('phone', 'true'))),
                'profil_email'   => trim(stripHTMLtags($this->input->post('email', 'true'))),
                'profil_update'  => date('Y-m-d H:i:s'),
            );
        }

        $this->db->where('profil_id', $profil_id);
        $this->db->update('my_profil', $data);
    }
}
/* Location: ./application/model/admin/Profil_m.php */
