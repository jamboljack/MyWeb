<section id="home" class="tt-fullHeight" data-stellar-vertical-offset="50" data-stellar-background-ratio="0.2">
    <div class="intro">
        <div class="intro-sub"><?=$setting->setting_title;?></div>
        <h1><?=$setting->setting_subtitle;?></h1>
        <p><?=$setting->setting_desc;?></p>
        <div class="social-icons">        
            <ul class="list-inline">
                <?php foreach($listSocial as $r) { ?>
                <li><a href="<?=$r->social_url;?>" target="_blank"><i class="fa fa-<?=$r->social_class;?>"></i></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="mouse-icon">
        <div class="wheel"></div>
    </div>
</section>

<section id="about" class="about-section section-padding">
    <div class="container">
        <h2 class="section-title wow fadeInUp">Kebijakan Privasi</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="short-info wow fadeInUp">
                    <p><?=$detail->privacy_desc;?></p>
                </div>
            </div>
        </div>
    </div>
</section>