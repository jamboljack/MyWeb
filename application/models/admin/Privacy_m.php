<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Privacy_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function select_detail($privacy_id = 1)
    {
        $this->db->select('*');
        $this->db->from('my_privacy');
        $this->db->where('privacy_id', $privacy_id);

        return $this->db->get();
    }

    public function update_data()
    {
        $privacy_id = 1;

        $data = array(
            'privacy_desc'   => $this->input->post('desc', 'true'),
            'privacy_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('privacy_id', $privacy_id);
        $this->db->update('my_privacy', $data);
    }
}
/* Location: ./application/models/admin/Privacy_m.php */
