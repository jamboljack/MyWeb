<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Privacy_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function select_detail()
    {
        $this->db->select('*');
        $this->db->from('my_privacy');
        $this->db->where('privacy_id', 1);

        return $this->db->get();
    }

    public function select_setting()
    {
        $this->db->select('*');
        $this->db->from('my_setting');
        $this->db->where('setting_id', 1);

        return $this->db->get();
    }

    public function select_social()
    {
        $this->db->select('*');
        $this->db->from('my_social');
        $this->db->order_by('social_id', 'asc');

        return $this->db->get();
    }
}
/* Location: ./application/model/Privacy_m.php */
